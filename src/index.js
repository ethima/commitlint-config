import baseConfiguration from "@commitlint/config-conventional/lib/index.js";
import { wildcards as defaultIgnoreFunctions } from "@commitlint/is-ignored/lib/defaults.js";

const footnoteMatcher = new RegExp("^\\[\\^.+?\\]:");

// Matches the header of commits representing semantic releases
const semanticReleaseCommitMatcher = new RegExp(
  "^(build|chore)\\(release\\):\\s*v?\\d+(\\.\\d+){2}.*$",
  "m",
);

// When referencing URIs in commit messages, it is very common to hit
// `max-line-length` restrictions. As a work-around, these URIs may be added as
// Markdown footnotes which can then be detected and given an exemption
const buildMaxLineLengthWithFootnoteExemption = (section) => {
  return (parsed, _, value) => {
    const sectionContent = parsed[section];

    if (!sectionContent) {
      return [true];
    }

    const lines = sectionContent.split(/\r?\n/);

    const result = lines.every(
      (line) => line.length <= value || footnoteMatcher.test(line),
    );

    return [
      result,
      `A ${section}'s lines must not be longer than ${value} characters`,
    ];
  };
};

// Only keep the default ignore functions allowing:
//
// - Merge/pull request merge commits into (pre)release branches.
// - Reapply commits (to allow reapplying previously reverted conventionally
//   documented changes).
// - Revert commits (to allow undoing/modifying previously conventionally
//   documented changes).
//
// Instead of duplicating these function definitions, it is more
// straightforward to obtain them from the defaults
const applicableDefaultIgnoreFunctions = defaultIgnoreFunctions.filter(
  (ignoreFunction) =>
    ignoreFunction("Reapply ") ||
    ignoreFunction("Revert ") ||
    ignoreFunction("Merge 'x' into 'y' "),
);

// Ignore functions specific to this commitlint configuration
const configurationSpecificIgnoreFunctions = [
  semanticReleaseCommitMatcher.test.bind(semanticReleaseCommitMatcher),
];

export default {
  // Do not use the default ignore patterns as they allow patterns, e.g.
  // `fixup!` and `squash!` commits, that should be blocked in the workflows
  // primarily supported by this configuration
  defaultIgnores: false,
  extends: ["@commitlint/config-conventional"],
  ignores: [
    ...applicableDefaultIgnoreFunctions,
    ...configurationSpecificIgnoreFunctions,
  ],
  plugins: [
    {
      rules: {
        "body-max-line-length-with-footnote-exemption":
          buildMaxLineLengthWithFootnoteExemption("body"),
        "footer-max-line-length-with-footnote-exemption":
          buildMaxLineLengthWithFootnoteExemption("footer"),
      },
    },
  ],
  // Disable the original `max-line-length` rules and replace them with
  // versions that have an exemption for footnotes
  rules: {
    "body-max-line-length": [0],
    "body-max-line-length-with-footnote-exemption":
      baseConfiguration.rules["body-max-line-length"],
    "footer-max-line-length": [0],
    "footer-max-line-length-with-footnote-exemption":
      baseConfiguration.rules["footer-max-line-length"],
  },
};
