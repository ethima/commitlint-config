import lint from "@commitlint/lint";
import load from "@commitlint/load";
import { describe, it } from "node:test";

import CONFIGURATION from "./index.js";

const lintWithParsedConfiguration = async (message) => {
  const parsedConfiguration = await load(CONFIGURATION);
  return await lint(message, parsedConfiguration.rules, parsedConfiguration);
};

const testMaximumLineLengthWithFootnoteExemption = async (
  t,
  prefix,
  section,
) => {
  const message = `build: a message with a potentially long line in the ${section}

A multi-line body to ensure proper detection of long lines without
interpreting multiple lines as a single line going over the limit.\n\n`;

  const exact_length_line = prefix + "x".repeat(100 - prefix.length);
  const exact_length_result = await lintWithParsedConfiguration(
    message + exact_length_line,
  );

  t.assert.ok(exact_length_result.valid);

  const too_long_line = prefix + "x".repeat(100);
  const too_long_result = await lintWithParsedConfiguration(
    message + too_long_line,
  );

  t.assert.ok(!too_long_result.valid);
  t.assert.ok(
    too_long_result.errors.some(
      ({ name }) =>
        name === `${section}-max-line-length-with-footnote-exemption`,
    ),
  );
  t.assert.ok(
    !too_long_result.errors.some(
      ({ name }) => name === `${section}-max-line-length`,
    ),
  );
  t.assert.ok(
    too_long_result.errors.some(({ message }) =>
      message.startsWith(`A ${section}'s`),
    ),
  );

  const footnote_line = `[^1]: ${prefix}${"x".repeat(101)}`;
  const footnote_result = await lintWithParsedConfiguration(
    message + footnote_line,
  );

  t.assert.ok(footnote_result.valid);
};

describe("The configuration", () => {
  it("is based on the 'conventional commits' configuration", (t) => {
    t.assert.ok(
      CONFIGURATION.extends.includes("@commitlint/config-conventional"),
    );
  });

  it("disallows `fixup!` commits", async (t) => {
    const result = await lintWithParsedConfiguration(
      "fixup! build(deps): commit title",
    );

    t.assert.ok(!result.valid);

    t.assert.ok(result.errors.some(({ name }) => name === "subject-empty"));
    t.assert.ok(result.errors.some(({ name }) => name === "type-empty"));
  });

  it("disallows bodies longer than 100 characters, except when a line is a footnote", async (t) => {
    const prefix = "Not a footer trigger ";
    await testMaximumLineLengthWithFootnoteExemption(t, prefix, "body");
  });

  it("disallows footers longer than 100 characters, except when a line is a footnote", async (t) => {
    // The `#` sign in the url triggers the prefix to be parsed as a "footer"
    const prefix = "Closes #123";
    await testMaximumLineLengthWithFootnoteExemption(t, prefix, "footer");
  });

  it("disallows `squash!` commits", async (t) => {
    const result = await lintWithParsedConfiguration(
      "squash! build(deps): commit title",
    );

    t.assert.ok(!result.valid);

    t.assert.ok(result.errors.some(({ name }) => name === "subject-empty"));
    t.assert.ok(result.errors.some(({ name }) => name === "type-empty"));
  });

  it("allows pull/merge request merge commits", async (t) => {
    const result = await lintWithParsedConfiguration("Merge 'foo' into 'main'");

    t.assert.ok(result.valid);
  });

  it("allows 'reapply' commits", async (t) => {
    const result = await lintWithParsedConfiguration(
      'Reapply "build(deps): commit title"',
    );

    t.assert.ok(result.valid);
  });

  it("allows 'revert' commits", async (t) => {
    const result = await lintWithParsedConfiguration(
      'Revert "build(deps): commit title"',
    );

    t.assert.ok(result.valid);
  });

  it("ignores semantic release commits", async (t) => {
    await Promise.all(
      ["build", "chore"].map(async (prefix) => {
        const result = await lintWithParsedConfiguration(
          `
${prefix}(release): 1.0.0

# 1.0.0 (2023-11-08)

### Features

* allow "revert" commits ([b580b7e](https://gitlab.com/ethima/commitlint-config/commit/b580b7e277ff638d27b2aa64a5ee45a302f82421))
* allow long-lines in commit message bodies if they are "footnotes" ([fff51c9](https://gitlab.com/ethima/commitlint-config/commit/fff51c9c799f0db2e4e57a422acb9ba4283e57c1))
`,
        );

        t.assert.ok(result.valid);
      }),
    );
  });
});
