# Changelog

## [2.1.2](https://gitlab.com/ethima/commitlint-config/compare/v2.1.1...v2.1.2) (2025-03-10)


### Performance Improvements

* **deps:** update commitlint monorepo to v19.8.0 ([f114b22](https://gitlab.com/ethima/commitlint-config/commit/f114b220e9859ceda1b0e073046cf142f7098386))

## [2.1.1](https://gitlab.com/ethima/commitlint-config/compare/v2.1.0...v2.1.1) (2025-02-06)


### Bug Fixes

* **deps:** update commitlint monorepo to v19.7.1 ([d08ddc9](https://gitlab.com/ethima/commitlint-config/commit/d08ddc97343498b5ee2707df8d64ee39cc1f2d36))

# [2.1.0](https://gitlab.com/ethima/commitlint-config/compare/v2.0.0...v2.1.0) (2024-11-20)


### Features

* allow "reapply" commits ([68f8d0f](https://gitlab.com/ethima/commitlint-config/commit/68f8d0f52ad9561e37f99a0ca71b9918ec1e2de4))

# [2.0.0](https://gitlab.com/ethima/commitlint-config/compare/v1.1.0...v2.0.0) (2024-10-29)


* feat!: migrate to ES modules ([e66694d](https://gitlab.com/ethima/commitlint-config/commit/e66694d620a617ad788f76b03ef3689d22c9d144))


### BREAKING CHANGES

* Migrate to ES modules. This requires v19 of all
`@commitlint` packages, including `@commitlint/cli`, for this
configuration to be usable.

# [1.1.0](https://gitlab.com/ethima/commitlint-config/compare/v1.0.0...v1.1.0) (2023-12-17)


### Features

* ignore semantic release commits ([6c21c73](https://gitlab.com/ethima/commitlint-config/commit/6c21c7370268de4385298ae019f7a0217b19c5de))

# 1.0.0 (2023-11-08)


### Features

* allow "revert" commits ([b580b7e](https://gitlab.com/ethima/commitlint-config/commit/b580b7e277ff638d27b2aa64a5ee45a302f82421))
* allow long-lines in commit message bodies if they are "footnotes" ([fff51c9](https://gitlab.com/ethima/commitlint-config/commit/fff51c9c799f0db2e4e57a422acb9ba4283e57c1))
* allow long-lines in commit message footers if they are "footnotes" ([159f2fa](https://gitlab.com/ethima/commitlint-config/commit/159f2fae4afe2b8cdf109c2dba4ef1f6039764ce))
* allow merge/pull request merge commits ([db52178](https://gitlab.com/ethima/commitlint-config/commit/db5217898e3af4d35954d33e44c27a2125716150))
* disallow `fixup!` and `squash!` commits ([7037358](https://gitlab.com/ethima/commitlint-config/commit/703735855183b6f6745a71ca4c6297b441b62332)), closes [40commitlint/is-ignored/src/defaults.ts#L23](https://gitlab.com/40commitlint/is-ignored/src/defaults.ts/issues/L23)
* extend the `@commitlint/config-conventional` configuration ([5e37887](https://gitlab.com/ethima/commitlint-config/commit/5e37887620b7a607605ea99e0b39a97e497603dc))
