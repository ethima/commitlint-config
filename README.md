# Commitlint Configuration

A shareable configuration for [Commitlint][commitlint-url] for projects
maintained by [the Ethima organization][ethima-url]. The configuration is based
on [the conventional commits][conventional-commits-url]
[configuration][commitlint-config-conventional-url], with some minor
adjustments as detailed below.

## Differences from the [`config-conventional` configuration][commitlint-config-conventional-url]

### More lenient rules regarding long lines in bodies and footers

When referencing URIs in commit messages it is trivial to trigger the
`{body,footer}-max-line-length` rules. This is a common scenario when
referencing code, commits, etc. on source hosting platforms.

One scenario in which long URIs in a commit message are common are release
commits of the semantic release process. Specifically linking to source commits
for changelog entries, see [the initial release of this configuration for an
example][ethima-commitlint-config-initial-release-commit-url]. These commits,
assumed to have a header of the form `build|chore(release): v<x.y.z>`, are
exempt from any checks.

Other long URI references can be included in commit messages by writing them as
Markdown footnotes, e.g. `[^1]: <long URI>`, `[^named-footnote]: <long URI>` at
the end of the commit message and referencing them from the body. Most hosting
platforms will render these footnotes with proper links, etc. in various places
in their UI. To support this functionality, this configuration replaces the
`{body,footer}-max-line-length` rules with the
`{body,footer}-max-line-length-with-footnote-exemption` rules. Apart from the
exemption, these rules behave the same as the original rules and accept the
same configuration. Both the `body-` and `footer-` rules are replaced as some
URIs may trigger the footer detection in the commit message parser,
particularly when a URI includes a `#`.

### Stricter rules on merge and `fixup!`/`squash!` commits

The desired workflow for projects using this configuration is to use feature
branches that are continuously rebased on top of (pre)release branches. This
includes the use of `fixup!` and `squash!` commits, which should all be rebased
out _prior_ to merging into a (pre)release branch. However, by default, these
types of commits are [allowed by the `is-ignored`
configuration][commitlint-is-ignored-fixup-squash-url], resulting in merge/pull
requests not being blocked when they should be by Commitlint.

Additionally, [the default `is-ignored`
configuration][commitlint-is-ignored-wildcards-url] allows different types of
merge commits which do not align well with the process set out here, e.g.
merging _into_ a feature branch or pulling updates using a merge strategy. All
of these commits should also be rebased out prior to merging into a
(pre)release branch.

The only acceptable commits that are ignored by default are those related to
"reverts", which may be used to update/undo conventionally documented changes
prior to a release and merge commits that result from merging feature branches
into (pre)release branches.

[commitlint-config-conventional-url]: https://www.npmjs.com/package/@commitlint/config-conventional
[commitlint-is-ignored-fixup-squash-url]: https://github.com/conventional-changelog/commitlint/blob/3bc4726f0dddfa051bc75f6af7b7ca076b04a839/%40commitlint/is-ignored/src/defaults.ts#L23
[commitlint-is-ignored-wildcards-url]: https://github.com/conventional-changelog/commitlint/blob/3bc4726f0dddfa051bc75f6af7b7ca076b04a839/%40commitlint/is-ignored/src/defaults.ts#L17-L29
[commitlint-url]: https://commitlint.js.org
[conventional-commits-url]: https://conventionalcommits.org/
[ethima-commitlint-config-initial-release-commit-url]: https://gitlab.com/ethima/commitlint-config/-/commit/3c32023a5647f8bf4090e1158a3d1f4fa9279324
[ethima-url]: https://gitlab.com/ethima/
